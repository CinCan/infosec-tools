/**
 * Fill the table
 */
function fillTable (tools) {
    let cat_filter = document.getElementById("menu_content");
    let tool_info = document.getElementById("tool_info");

    let table = new Tabulator("#tool_list", {
        layout: "fitDataFill",
        selectable: 1,
        columns:[
            {title:"#", field:"aggr#", sorter:"number", sorterParams: {alignEmptyValues: "bottom"}},
            {title:"Tool", field:"tool"},
            {title:"Name", field:"nick#", sorter:"number", sorterParams: {alignEmptyValues: "bottom"}},
            {title:"GitHub", field:"github#", sorter:"number", sorterParams: {alignEmptyValues: "bottom"}},
            {title:"Cross", field:"cross#", sorter:"number", sorterParams: {alignEmptyValues: "bottom"}},
            {title:"Categories", field:"cats"}
        ],
        rowSelected: function(row) { showToolInfo(row) },
        data: tools
    });

    /**
     * Show/clear tool information in footer
     */
    function showToolInfo(row) {
        urls = document.getElementById("tool_urls");
        tool_name = document.getElementById("tool_name")
        tool_nick = document.getElementById("tool_nick")
        urls.innerText = '';
        if (row == null) {
            tool_name.textContent = ''
            tool_nick.textContent = ''
            return
        }
        let data = row.getData();
        //console.log("Selected: " + data.tool);
        tool_name.textContent = data.name;
        tool_nick.textContent = data.tool;
        url_list = data.urls
        url_list.forEach(function(u, i) {
            a = document.createElement("a");
            a.textContent = u;
            a.href = u;
            a.target = "_blank";
            urls.appendChild(a);
            if (i < url_list.length - 1) {
                space = document.createTextNode(", ")
                urls.appendChild(space)
            }
        })
    }

    // Collect all categories
    cats = new Set()
    tools.forEach(function(t) {
        t.cats.forEach(function (c) {cats.add(c)})
    })
    cat_checks = new Map()
    // Create category filtering
    cats.forEach(function(cat) {
        let checkbox = document.createElement('input');
        checkbox.type = "checkbox";
        checkbox.name = cat;
        checkbox.id = cat;
        checkbox.onchange = onCategoryToggle
        cat_checks.set(cat, checkbox)  // build the map
        label = document.createElement('label')
        label.textContent = cat
        label.htmlFor = cat
        span = document.createElement('div')
        span.id = "cat_filter_element"
        span.appendChild(checkbox)
        span.appendChild(label)
        cat_filter.appendChild(span)
    })
    checked = new Set()

    // search term
    searchTerm = ''
    search_field = document.getElementById("search_field")
    /**
     * Search field update
     */
    search_field = document.getElementById("search_field")
    onSearchChange = function(e) {
        searchTerm = search_field.value.toLowerCase()
        showToolInfo(null)  // clear
        table.setFilter(tableFilter)
    }
    search_field.addEventListener('search', onSearchChange);

    /**
     * Table filter callback
     */
    function tableFilter(row) {
        if (searchTerm) {
            if (!row.tool.includes(searchTerm)) {
                return false
            }
        }
        if (checked.size == 0) {
            return true // all categories unchecked
        }
        show_row = false;
        row.cats.forEach(function(c) {
            show_row = show_row || checked.has(c)
        })
        return show_row
    }

    /**
     * Toggling filter checkbox
     */
    function onCategoryToggle(cb) {
        checked.clear();
        cat_checks.forEach(function(c, cat) {
            if (c.checked) {
                checked.add(cat)
            }
        })
        showToolInfo(null)  // clear
        table.setFilter(tableFilter)
    }


}

/**
 * Menu toggling show/hide
 */
function onFilterMenuClick() {
    menu = document.getElementById("menu_content");
    if (menu.style.display != 'block') {
        menu.style.display = 'block';
    } else {
        menu.style.display = 'none';
    }
}
