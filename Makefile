.PHONY: public

# Finish public/ directory
public: public/dist/tabulator public/tools.json

# Tools JSON
public/tools.json: tools.json
	cp $^ $@

# Tabulator
public/dist/tabulator: packages/tabulator
	mkdir -p $@
	cp packages/tabulator/LICENSE $@
	cp packages/tabulator/dist/js/tabulator.min.js $@
	cp packages/tabulator/dist/css/tabulator.min.css $@

packages/tabulator:
	mkdir -p $@
	cd packages && git clone https://github.com/olifolkerd/tabulator.git

# Clean up
clean:
	rm -rf public/tools.json public/dist
	rm -rf packages
