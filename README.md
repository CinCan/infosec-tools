# Infosec Tools

This repository holds data for article "100 Popular Open-Source Infosec Tools" published in proceedings of ICT Systems Security and Privacy Protection 36th IFIP TC 11 International Conference, SEC 2021, Oslo, Norway, June 22–24, 2021.

Data can be browsed at https://cincan.gitlab.io/infosec-tools/

The raw data is available here as [JSON file](tools.json).



